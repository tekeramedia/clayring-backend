const createError = require('http-errors')
const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const dotenv = require('dotenv')
const db = require('./connection')
const ApiRouter = require('./routes/Api')
const AuthRouter = require('./routes/Auth')
const AdminAuth = require('./routes/AdminAuthRoutes')
const indexRouter = require('./routes/index')
const cors = require('cors')

const app = express()

app.use(cors())

// config .env file
dotenv.config({ path: './config.env' })

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'jade')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

db.on('error', console.error.bind(console, 'error connecting to database'))
db.on('connected', () => {
  console.log('connected to database')
})

app.use('/', indexRouter)
app.use('/api/v1', ApiRouter)
app.use('/auth', AuthRouter)
app.use('/admin/auth', AdminAuth)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404))
})

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app
