const Admin = require('../../models/Admin')
const bcrypt = require('bcryptjs')
const AdminToken = require('../../models/AdminToken')
const jwt = require('jsonwebtoken')

const generateAccessToken = (admin) => {
    return jwt.sign({ id: admin._id, email: admin.email, fullname: admin.fullname }, process.env.ACCESS_TOKEN)
}

const generateRefreshToken = (admin) => {
    return jwt.sign({ id: admin._id, email: admin.email, fullname: admin.fullname }, process.env.REFRESH_TOKEN)
}

exports.signup = async (req, res) => {
    const { email, username, password } = req.body

    const adminExists = await Admin.findOne({ email })

    adminExists ? res.status(403).json({ message: 'This email has already been registered' }) : null

    const encryptPassword = await bcrypt.hash(password, 14)

    const admin = new Admin({
        username: username,
        email: email,
        password: encryptPassword
    })

    try {
        await admin.save()
        res.status(200).json({ msg: 'admin registered successfully' })
    } catch (err) {
        res.status(400).json({ error: `error registering: ${err}` })
    }

}

exports.signin = async (req, res) => {

    const { email, password } = req.body

    const admin = await Admin.findOne({ email })

    !admin ? res.status(403).json({ message: 'This account does not exist' }) : null

    bcrypt.compare(password, admin.password, async (err, match) => {
        if (match) {

            const accessToken = generateAccessToken(admin)
            const refreshToken = generateRefreshToken(admin)

            const token = new AdminToken({
                token: refreshToken
            })
            token.save()

            res.json({
                id: admin._id,
                username: admin.username,
                email: admin.email,
                accessToken,
                refreshToken
            })

        } else {
            res.status(403).json({ msg: "invalid password" })
        }
    })

}

exports.signout = async (req, res, next) => {
    const refreshToken = req.body.token
    try {
        await AdminToken.deleteOne({ token: refreshToken })
        res.status(200).json({ msg: "user logged out successfully" })
    } catch (err) {
        res.status(401).send({ "error": "error deleting" })
    }
}
