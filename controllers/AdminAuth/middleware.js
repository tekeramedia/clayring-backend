const jwt = require('jsonwebtoken')
const verifyAdmin = (req, res, next) => {

    const authHeader = req.headers.token

    if (authHeader) {
        const token = authHeader.split(" ")[1]
        jwt.verify(token, process.env.ACCESS_TOKEN, (err, admin) => {
            err ? res.status(403).json("invalid token") : null
            req.admin = admin
            next()
        })
    }
    else {
        res.status(401).json("you're not authenticated as an admin")
    }
}

module.exports = { verifyAdmin }
