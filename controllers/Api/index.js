const Customer = require('../../models/Customer')
const Info = require('../../models/Info')
const Payment = require('../../models/PaymentInfo')

exports.customers_list = async (req, res) => {
    try {
        const result = await Customer.find({}, { "fullname": 1, "email": 1, "_id": 1, "id": 1 })
        res.status(200).json(result)
    } catch (err) {
        res.status(500).json({ error: 'error connecting' })
    }
}

exports.get_one_customer = async (req, res) => {
    try {
        const result = await Customer.findById(req.params.id, { "firstname": 1, "email": 1, "_id": 1 })
        res.status(200).json(result)
    } catch (err) {
        res.status(400).json({ error: 'cannot get data' })
    }
}


exports.create_info = async (req, res) => {
    
    const kin = {
        name: req.body.kinFullname,
        address: req.body.kinAddress,
        phone: req.body.kinPhone,
        email: req.body.kinEmail,
    }

    const employment = {
        name: req.body.empName,
        occupation: req.body.occupation,
        phone: req.body.empTel,
        address: req.body.empAddress,
    }

    const purchase = {
        shop_use: req.body.shopUse,
        shop_selection: req.body.shopSelect
    }

    const payment = {
        agreed_price: req.body.agreedPrice,
        payment_mode: req.body.paymentMode
    }

    try {
        const info = new Info({
            customerId: req.body.customerId,
            fullname: req.body.fullname,
            address: req.body.address,
            cpaddress: req.body.cpaddress,
            gender: req.body.gender,
            status: req.body.status,
            phone: req.body.phone,
            mobile: req.body.mobile,
            email: req.body.email,
            kin: kin,
            employment_details: employment,
            purchase_details: purchase,
            payment_details: payment,
            medium: req.body.medium
        })
        const response = await info.save()
        res.status(200).json(response)

    } catch (err) {
        res.status(403).json({ error: "error creating info" })
        console.log(err)
    }
}

exports.get_info = async (req, res) => {
    try {
        const result = await Info.findOne({ customerId: req.params.customerId })
        res.status(200).json(result)
    } catch (err) {
        res.status(403).json({ error: 'cannot get data' })
    }
}


exports.update_payment_info = async (req, res) => {
    const { customerId, total, paid, balance, next_payment, span } = req.body
    try {
        let customer = await Payment.findOne({ customerId: customerId })
        if (customer) {
            const UPDATE = {
                total: total,
                paid: paid,
                balance: balance,
                next_payment: next_payment,
                span: span
            }
            customer = UPDATE
            customer.save()
        }
    } catch (error) {
        res.status(400).json({ error: 'error updating payment info' })
    }
}
