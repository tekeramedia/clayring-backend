const Customer = require('../../models/Customer')
const Token = require('../../models/Token')
const Verifier = require('../../models/Verifier')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const sgEmail = require('@sendgrid/mail')

const generateAccessToken = (user) => {
    return jwt.sign({ id: user._id, email: user.email, fullname: user.fullname }, process.env.ACCESS_TOKEN)
}

const generateRefreshToken = (user) => {
    return jwt.sign({ id: user._id, email: user.email, fullname: user.fullname }, process.env.REFRESH_TOKEN)
}

exports.register = async (req, res) => {

    const { fullname, email, password } = req.body

    const exist = await Customer.findOne({ email })

    exist ? res.status(403).json({ warning: 'The email has already been registered' }) : null

    const encryptedPassword = await bcrypt.hash(password, 14)

    const customer = new Customer({
        fullname: fullname,
        email: email,
        password: encryptedPassword
    })

    try {
        await customer.save()
        res.status(200).json({ success: 'Registered sucessfully' })
    } catch (error) {
        res.status(400).json({ error })
    }

    // try {
    //     const response = await customer.save()
    //     const randomDigits = Math.floor(Math.random() * 9000) + 1000
    //     const verifier = new Verifier({
    //         customerId: response._id,
    //         digits: randomDigits
    //     })
    //     await verifier.save()
    //     const msg = {
    //         from: 'noreply@clayring.com',
    //         to: response.email,
    //         subject: 'Clayring - Verify your email',
    //         text: `
    //         Hello ${response.fullname} thanks for registering on Clayring,
    //         please paste the code below to verify your account.
    //         ${randomDigits}
    //         `,
    //         html: `
    //         <h1>Hello ${response.fullname}</h1>
    //         <p>Thanks for registering on Clayring,
    //         please paste the code below to verify your account.</p> <br />
    //         <h2>${randomDigits}</h2>
    //         `
    //     }
    //     await sgEmail.send(msg)
    //     res.status(200).json({ success: 'Registered sucessfully' })
    // } catch (error) {
    //     res.status(400).json(error)
    //     console.log(error)
    // }
}

exports.login = async (req, res) => {

    const { email, password } = req.body

    const user = await Customer.findOne({ email })

    !user ? res.status(403).json({ warning: 'This account does not exist' }) : null

    bcrypt.compare(password, user.password, async (err, match) => {
        if (match) {
            const accessToken = generateAccessToken(user)
            const refreshToken = generateRefreshToken(user)

            const token = new Token({
                token: refreshToken
            })

            await token.save()

            res.json({
                id: user._id,
                fullname: user.fullname,
                email: user.email,
                accessToken,
                refreshToken
            })
        } else {
            res.status(403).json({ error: "invalid password" })
        }
    })

}

exports.logout = async (req, res, next) => {
    const refreshToken = req.body.token
    try {
        await Token.deleteOne({ token: refreshToken })
        res.status(200).json({ msg: "user logged out successfully" })
    } catch (err) {
        res.status(401).send({ error: "error logging out" })
    }
}

exports.refresh = async (req, res) => {
    const refreshToken = req.body.token

    !refreshToken ? res.status(401).json({ error: "you're not authenticated" }) : null
    const isTokenExist = await Token.findOne({ token: refreshToken })
    if (!isTokenExist) {
        return res.status(403).json({ error: "refresh token is not valid" })
    } else {
        jwt.verify(refreshToken, process.env.REFRESH_TOKEN, (err, user) => {
            err && console.log(err)
            Token.findOneAndDelete({ token: refreshToken })

            const newAccessToken = generateAccessToken(user)
            const newRefreshToken = generateRefreshToken(user)

            res.status(200).json({
                accessToken: newAccessToken,
                refreshToken: newRefreshToken
            })
        })
    }
}

exports.verify_email = async (req, res) => {
    const { customerId, digits } = req.body
    const customerIsVerified = await Verifier.findOne({ customerId: customerId, digits: digits })
    if (customerIsVerified) {
        const customer = await Customer.findOne({ _id: customerId })
        customer.verified = true
        customer.save()
    }
}
