const jwt = require('jsonwebtoken')
const verify = (req, res, next) => {

    const authHeader = req.headers.token

    if (authHeader) {
        const token = authHeader.split(" ")[1]
        jwt.verify(token, process.env.ACCESS_TOKEN, (err, customer) => {
            err ? res.status(403).json("invalid token") : null
            req.customer = customer
            next()
        })
    }
    else {
        res.status(401).json({ error: "you're not authenticated" })
    }
}

module.exports = { verify }
