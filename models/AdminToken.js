const mongoose = require("mongoose")

const Schema = mongoose.Schema

const AdminTokenSchema = new Schema({
    token: { type: String, required: true }
}, { timestamps: true })

module.exports = mongoose.model('AdminToken', AdminTokenSchema)
