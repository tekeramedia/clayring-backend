const mongoose = require('mongoose')
const { MongooseAutoIncrementID } = require('mongoose-auto-increment-reworked')

const Schema = mongoose.Schema

const CustomerSchema = new Schema({
    fullname: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    verified: { type: Boolean, default: false },
    id: { type: Number },
}, { timestamps: true })

MongooseAutoIncrementID.initialise('counters');

CustomerSchema.plugin(MongooseAutoIncrementID.plugin, {
    modelName: 'Customer',
    field: 'id',
    incrementBy: 1,
    startAt: 1,
    unique: true,
    nextCount: false,
    resetCount: false,
});

module.exports = mongoose.model('Customer', CustomerSchema)
