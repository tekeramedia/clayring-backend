const mongoose = require('mongoose')

const Schema = mongoose.Schema

const NextKin = new Schema({
    name: { required: true, type: String },
    address: { required: true, type: String },
    phone: { required: true, type: String },
    email: { required: true, type: String },
})

const Employment = new Schema({
    name: { type: String, required: true },
    occupation: { type: String, required: true },
    phone: { type: String, required: true },
    address: { type: String, required: true },
})

const Purchase = new Schema({
    shop_use: { type: String, required: true },
    shop_selection: { type: String },
})

const Payment = new Schema({
    agreed_price: { type: String, required: true },
    payment_mode: { type: String, required: true },
})


const InfoSchema = new Schema({
    customerId: { type: Schema.Types.ObjectId, ref: 'Customer', required: true },
    fullname: { type: String, required: true },
    address: { type: String, required: true },
    cpaddress: { type: String, required: true },
    gender: { type: String, required: true },
    status: { type: String, required: true },
    phone: { type: String, required: true },
    mobile: { type: String, required: true },
    email: { type: String, required: true },
    kin: [NextKin],
    employment_details: [Employment],
    purchase_details: [Purchase],
    payment_details: [Payment],
    medium: { type: String, required: true }
})

module.exports = mongoose.model('Info', InfoSchema)
