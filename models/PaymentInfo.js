const mongoose = require('mongoose')
const Schema = mongoose.Schema

const PaymentInfoSchema = new Schema({
    customerId: { type: Schema.Types.ObjectId, ref: 'Customer', required: true },
    total: { type: Number, required: true },
    paid: { type: Number, required: true },
    balance: { type: Number, required: true },
    next_payment: { type: Date },
    span: { type: Number, default: 0 },
}, { timestamps: true })

module.exports = mongoose.model('PaymentInfo', PaymentInfoSchema)
