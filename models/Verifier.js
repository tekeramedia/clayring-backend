const mongoose = require('mongoose')
const Schema = mongoose.Schema

const VerifierSchema = new Schema({
    customerId: { type: Schema.Types.ObjectId, ref: 'Customer', required: true },
    digits: { type: Number, required: true }
}, { timestamps: true })

module.exports = mongoose.model('Verifier', VerifierSchema)
