const express = require('express')
const router = express.Router()
const { signin, signup, signout } = require('../../controllers/AdminAuth')
const { verifyAdmin } = require('../../controllers/AdminAuth/middleware')

router.post('/signin', signin)

router.post('/signup', signup)

router.post('/signout', verifyAdmin, signout)

module.exports = router
