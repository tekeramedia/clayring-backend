const express = require('express')
const router = express.Router()
const customer_controller = require('../../controllers/Api')
const { verify } = require('../../middlewares')


router.get('/customers', customer_controller.customers_list)

router.get('/customers/:id', customer_controller.get_one_customer)

router.post('/info', verify, customer_controller.create_info)

module.exports = router
