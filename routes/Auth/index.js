const express = require('express')
const router = express.Router()
const { register, login, logout, refresh, verify_email } = require('../../controllers/Auth')
const { verify } = require('../../middlewares')

router.post('/register', register)

router.post('/login', login)

router.post('/logout', verify, logout)

router.post('/refresh', refresh)

router.post('/verify', verify_email)

module.exports = router
